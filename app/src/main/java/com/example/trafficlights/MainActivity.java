package com.example.trafficlights;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    LinearLayout mLinearLayout;
    Button btn_red;
    Button btn_yellow;
    Button btn_green;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLinearLayout = (LinearLayout) findViewById(R.id.relativeLayout);
        btn_red = (Button) findViewById(R.id.btn_red);
        btn_yellow = (Button) findViewById(R.id.btn_yellow);
        btn_green = (Button) findViewById(R.id.btn_green);
    }
    public void onClickBtnAddColorRed(View view){
        mLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorRed, null));
    }
    public void onClickBtnAddColorYellow(View view){
        mLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorYellow, null));
    }
    public void onClickBtnAddColorGreen(View view){
        mLinearLayout.setBackgroundColor(getResources().getColor(R.color.colorGreen, null));
    }
}